#!/usr/bin/guile \
-L . -e main -s
!#

(add-to-load-path (dirname (current-filename)))

(use-modules (dns)
             (rnrs bytevectors)
             (ice-9 format)
             ((texinfo string-utils) :select (left-justify-string))
             (ice-9 getopt-long))

(define get-id
  (let ((i 100))
    (lambda () (set! i (1+ i)) i)))

(define (setup-sock addr)
  (define ai (car (getaddrinfo addr "domain" 0 0 SOCK_DGRAM)))
  (define sock (socket (addrinfo:fam ai)
                       (addrinfo:socktype ai)
                       (addrinfo:protocol ai)))
  (connect sock (addrinfo:addr ai))
  sock)

(define (run sock msg)
  (define resp (make-bytevector 512))
  (send sock (pack-dns-message msg))
  (recv! sock resp)
  (unpack-dns-message resp))





(define* (format-header header optional: (port #t))
  (format port ";; ->>HEADER<<- opcode: ~a, status: ~a, id: ~a~%"
          (opcode header) (rcode header) (id header))
  (format port ";; flags:~{ ~a~}; QUERY: ~a, ANSWER: ~a, AUTHORITY: ~a, ADDITIONAL: ~a~%"
          (list (if (aa header) 'aa "")
                (if (tc header) 'tc "")
                (if (rd header) 'rd "")
                (if (ra header) 'ra ""))
          (qdcount header)
          (ancount header)
          (nscount header)
          (arcount header)))

(define* (format-question question optional: (port (current-output-port)))
  (display ";" port)
  (display (left-justify-string (format #f "~a.~/" (name question)) 23) port)
  (format port "~/~/~a~/~a~%"
          (class question)
          (type question)))

(define* (format-record rr optional: (port (current-output-port)))
  (display (left-justify-string (format #f "~a.~/" (name rr)) 24) port)
  (format port "~a~/~a~/~a~/~a~%"
          (ttl rr)
          (class rr)
          (type rr)
          (rdata rr)))

;; Both question and answer should me dns-message objects
(define* (format-everything question answer optional: (port (current-output-port)))
  (format-header (header question) port)
  (format-header (header answer) port)
  (format port "~%;; QUESTION SECTION:~%")
  (for-each format-question (questions question))
  (format port "~%;; ANSWER SECTION:~%")
  (for-each format-record (answers answer))
  (format port "~%;; AUTHORITY SECTION:~%")
  (for-each format-record (authorities answer))
  (format port "~%;; ADDITIONAL SECTION:~%")
  (for-each format-record (additionals answer))
  )



(define option-spec
  '((server (value #t))))

(define (main args)
  (define options (getopt-long args option-spec
                               stop-at-first-non-option: #f))
  (define-values (what type)
    (let ((rest (option-ref options '() '())))
      (cond ((null? rest) (exit 1))
            ((= 1 (length rest)) (values (car rest) 'A))
            (else (values (car rest)
                          (string->symbol (cadr rest)))))))

  (define msg
    (make-dns-message
     header: (make-dns-header id: (get-id) rd: #t qdcount: 1)
     questions: (list (make-dns-question name: what type: type))))

  (define dns-server-address
    (or (option-ref options 'server #f)
        (car ((@ (dns resolvconf) resolvconf)))))

  (format-everything msg (run (setup-sock dns-server-address) msg))
  (newline)
  (format #t ";; Server: ~a (UDP)~%" dns-server-address))
