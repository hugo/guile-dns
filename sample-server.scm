
(use-modules (rnrs bytevectors)
             (srfi srfi-1)
             (srfi srfi-71)
             (dns))

(define ai
 (car
  (getaddrinfo "::" "5353"
               AI_PASSIVE
               0
               SOCK_DGRAM
               )))

(define sock
 (socket (addrinfo:fam ai)
         (addrinfo:socktype ai)
         (addrinfo:protocol ai)))

(bind sock (addrinfo:addr ai))

(define bv (make-bytevector 512))
(let ((_ return-addr (car+cdr (recvfrom! sock bv))))
  (let ((q-message (unpack-dns-message bv)))
    (let ((r-message
           (make-dns-message
            header: (make-dns-header id: (id (header q-message))
                                     qr: #t
                                     opcode: 'QUERY
                                     qdcount: 1
                                     ancount: 1
                                     arcount: 0
                                     )
            questions: (questions q-message)
            answers: (list (make-dns-rr-data
                            name: (name (car (questions q-message)))
                            type: (type (car (questions q-message)))
                            class: (class (car (questions q-message)))
                            ttl: 300
                            rdata: (assoc-ref
                                    ;; Fix responses for each query type.
                                    ;; Mostly here to check that each type gets properly serialized.
                                    '((A     . "203.0.113.10")
                                      (AAAA  . "2001:db8::1")
                                      (CNAME . "example.com")
                                      (TXT   . "Example.string.")
                                      )
                                    (type (car (questions q-message))))))
            ; additionals: (additionals q-message)
            )))
      (let ((bv (pack-dns-message r-message)))
        (sendto sock bv return-addr)))))
