.PHONY: all install check clean

# Tests are run in the order noted here
tests = state.scm \
	bv.scm \
	label.scm \
	enum.scm \
	reverse.scm \
	object.scm \
	types-round-trip.scm \
	header.scm \
	rr.scm \

sources = dns.scm \
	$(shell find dns -type f -name \*.scm)

targets = $(sources:%.scm=obj/%.go)
# This is sort of a hack for the install target
install_targets = $(sources:%.scm=%.go)

GUILE = guile

GUILE_VERSION = $(shell guile -c '(display (effective-version))')

BUILD_ENV = GUILE_AUTO_COMPILE=0

TEST_ENV = GUILE_AUTO_COMPILE=0 \
	GUILE_LOAD_PATH=$(CURDIR) \
	GUILE_LOAD_COMPILED_PATH=$(CURDIR)/obj
TEST_FLAGS = $(if $(VERBOSE),--verbose) $(if $(COVERAGE),--coverage=coverage.info)

PREFIX = /usr
DATAROOTDIR=$(PREFIX)/share
INFODIR=$(DATAROOTDIR)/info

GUILE_CFLAGS = -O2 --warn=unused-variable,unused-toplevel,shadowed-toplevel,unbound-variable,macro-use-before-definition,arity-mismatch,duplicate-case-datum,bad-case-datum,format

obj/%.go: %.scm
	env $(BUILD_ENV) guild compile -L $(CURDIR) -o $@ $<

all: $(targets) doc/guile-dns.info

install: all
	install -m644 -D -t $(DESTDIR)/$(INFODIR) doc/guile-dns.info
	for file in $(sources); do \
		install -m644 -D -t $(DESTDIR)/$(PREFIX)/share/guile/site/$(GUILE_VERSION)/$$(dirname $$file) $$file; \
	done
	for file in $(install_targets); do \
		install -m644 -D -t $(DESTDIR)/$(PREFIX)/lib/guile/$(GUILE_VERSION)/site-ccache/$$(dirname $$file) obj/$$file; \
	done

check: all
	env $(TEST_ENV) $(GUILE) -e main -s run-tests.scm $(TEST_FLAGS) $(tests)

coverage/index.html: coverage.info
	genhtml --output-directory coverage $<

clean:
	-rm -r coverage
	-rm -r obj
