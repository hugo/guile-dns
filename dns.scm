(define-module (dns)
  :use-module (dns types message)
  :use-module (dns types header)
  :use-module (dns types question)
  :use-module (dns types rr)
  :use-module (rnrs bytevectors)
  :export (pack-dns-message
           unpack-dns-message)
  :re-export (dns-message->bytes
              bytes->dns-message
              make-dns-message  dns-message?
              make-dns-header   dns-header?
              make-dns-question dns-question?
              make-dns-rr-data  dns-rr-data?
              ))

(module-use! (module-public-interface (current-module))
             (resolve-interface '(dns types)))


(define (pack-dns-message msg)
  (call-with-values (lambda () ((dns-message->bytes msg) (make-bytevector 512) 0))
    (lambda (_ bv ptr) (pack-bv bv ptr))))


(define (unpack-dns-message bv)
  (call-with-values (lambda () ((bytes->dns-message bv) 0))
    (lambda (resp _) resp)))


(define (pack-bv bv size)
  (let ((return (make-bytevector size)))
    (bytevector-copy! bv 0 return 0 size)
    return))
