(define-module (dns util)
  :export (domain-join
           uint? u16?))

(define (domain-join lst)
  (string-join lst "."))


(define (uint? x max)
  (and (exact-integer? x)
       (<= 0 x (1- (expt max 16)))))

(define (u16? x)
  (uint? x 16))
