;;; Commentary:
;;; Another definition of define-record-type.
;;; Aims to be syntastically compatible with (rnrs records syntastic),
;;; But instead declares the object as a GOOPS object,
;;; creates a constructor which takes keyword arguments,
;;; sets a propper write method,
;;; and names all its getters directly be their fields
;;; Code:

(define-module (dns internal object)
  :use-module (rnrs base)
  :use-module (oop goops)
  :use-module (dns internal util)
  :use-module ((srfi srfi-1) :select (filter-map))
  :use-module (srfi srfi-88)
  ;; Define-record-type exported and not #:replace:d, since we want a warning if
  ;; multiple instances of it is imported at once.
  :export (define-record-type))

;;; Internal method ONLY used for `write'.
(define-generic serialize)

(define-method (serialize (this <top>))
  this)

(define-method (serialize (this <vector>))
  (vector-map serialize this))

(define-method (serialize (this <null>))
  #())

(define-method (serialize (this <pair>))
  (list->vector (map serialize this)))

(define-method (serialize (this <hashtable>))
  (hash-map->list (lambda (key value)
                    (list key (serialize value)))
                  this))




(define (handle-define*-clause stx)
  (syntax-case stx ()
    ((name default: default args ...) #'(name default))
    ((name arg args ...) (handle-define*-clause #'(name args ...)))
    ((name) #'name)
    (name   #'name)))

(define-syntax build-validator
  (syntax-rules (and or)
    ((_ variable (and clauses ...))  (and (build-validator variable clauses) ...))
    ((_ variable (or clauses ...))   (or (build-validator variable clauses) ...))
    ((_ variable (proc args ...))    (proc variable args ...))
    ((_ variable proc)               (proc variable))))

;; Given #'(<field-name> type: <validator-body>), expands validator-body to contain <field-name>
;; string x field-spec → <validator syntax>
(define (handle-validator constructor-name)
  (define (inner field)
    (syntax-case field ()
      ((name type: validator-body args ...)
       #`(unless (build-validator name validator-body)
           (scm-error 'wrong-type-arg #,constructor-name
                      "`~a: ~s' doesn't satisfy `~a'"
                      (list (quote name) name (quote validator-body))
                      (list (quote name) name))))
      ((name arg args ...)
       (inner #'(name args ...)))
      ;; Case when no #:type annotation exists.
      (_ #f)))
  inner)


;; Takes a field from the define-record-type macro, and returns the field name.
;; E.g.
;; (x args ...) ⇒ x
;; x ⇒ x
(define (field-name stx)
  (syntax-case stx ()
    ((name args ...) #'name)
    (name #'name)))

;; Helper function to use with with-syntax
(define (binding pattern fmt)
  (->> pattern
       syntax->datum
       (format #f fmt)
       string->symbol
       (datum->syntax pattern)))

(define-syntax define-record-type
  (lambda (stx)
    (syntax-case stx (fields)
      ((_ type (fields field ...))
       (with-syntax ((make-<type> (binding #'type "make-~a"))
                     (<type>?     (binding #'type "~a?")))
         #`(begin
             ;; construct class
             (define-class type ()
               ;; needs a pre-expansion since define-class is a macro in itself
               #,@(map (lambda (f) (with-syntax ((ff (field-name f)))
                                #'(ff getter: ff)))
                       #'(field ...)))

             ;; construct predicate
             (define (<type>? x)
               (is-a? x type))

             ;; construct public-facing constructor
             (define* (make-<type> key: #,@(map handle-define*-clause #'(field ...)))
               #,@(filter-map (handle-validator (symbol->string (syntax->datum #'make-<type>)))
                              #'(field ...))

               ;; bind all values to object
               (let ((return-value (make type)))
                 #,@(map (lambda (g)
                           (with-syntax ((f (field-name g)))
                             #`(slot-set! return-value (quote #,(field-name g)) f)))
                         #'(field ...))
                 return-value))

             (define-method (equal? (a type) (b type))
               (and #,@(map (lambda (g)
                              #`(equal? (#,(field-name g) a)
                                        (#,(field-name g) b)))
                            #'(field ...))))

             ;; pretty printing
             (define-method (serialize (this type))
               (quasiquote
                #,(map (lambda (g)
                         (with-syntax ((f (field-name g)))
                           #`(#,(field-name g) ,(serialize (f this)))))
                       #'(field ...))))

             (define-method (write (this type) port)
               ((@ (ice-9 format) format) port "#<~a~:{ ~a=~s~}>"
                (class-name type)
                (serialize this)))))))))
