;;; Commentary:
;;; These procedures uses our state monad to read bytes from a bytevector, while
;;; keeping a seek pointer as the implicit explicit state.
;;;
;;; So all use the state as the offset into the vector, and "sets" the state to
;;; the new offset after the read.
;;; Code:

(define-module (dns internal bv)
  :use-module (ice-9 curried-definitions)
  :use-module (dns internal state-monad)
  :use-module (rnrs bytevectors)
  :use-module (srfi srfi-88)
  :export (bv-copy!
           u8! u16! u32! uint-set!
           u8  u16  u32 bv-get uint-get
              )
  )


;;; The procedures u8, u16, and u32 reads an unsigned integer of the respective
;;; width from @var{bv}, and updates the seek pointer appropriately.


(define ((u8 bv) ptr)
  (values (bytevector-u8-ref bv ptr)
          (1+ ptr)))

;; Endianness is big by default for these, since we are working with network
;; stuff.

(define* ((u16 bv optional: (e (endianness big))) ptr)
  (values (bytevector-u16-ref bv ptr e)
          (+ ptr 2)))

(define* ((u32 bv optional: (e (endianness big))) ptr)
  (values (bytevector-u32-ref bv ptr e)
          (+ ptr 4)))

;; Copies @var{len} bytes from @var{bv}, and updates the seek pointer
;; accordingly.
(define ((bv-get bv len) ptr)
  (define ret (make-bytevector len))
  (bytevector-copy! bv ptr
                    ret 0
                    len)
  (values ret (+ ptr len)))

;; Reads an unsigned integer @var{len} octets wide, and updates the seek
;; pointer accordingly
(define* (uint-get bv len optional: (e (endianness big)))
  (do vec <- (bv-get bv len)
      (return (bytevector-uint-ref vec 0 (endianness big) len))))

(define (double-bv bv)
  "Return a fresh bytevector twice the size of the given, contents copied over."
  (define new-bv (make-bytevector (* 2 (max 1 (bytevector-length bv)))))
  (bytevector-copy! bv 0 new-bv 0 (bytevector-length bv))
  new-bv)

(define (with-resizing-bv bv proc)
  (catch 'out-of-range
    (lambda () (proc bv) bv)
    (lambda (err err-proc msg arg data)
      ;; Call ourselves recursivly, if index is way out of bounds
      (with-resizing-bv (double-bv bv) proc))))

(define (bytevector-u8-set-safe! bv ptr item)
  (with-resizing-bv bv (lambda (bv) (bytevector-u8-set! bv ptr item))))

(define* (bytevector-u16-set-safe! bv ptr item optional: (e (endianness big)))
  (with-resizing-bv bv (lambda (bv) (bytevector-u16-set! bv ptr item e))))

(define* (bytevector-u32-set-safe! bv ptr item optional: (e (endianness big)))
  (with-resizing-bv bv (lambda (bv) (bytevector-u32-set! bv ptr item e))))

(define (bytevector-copy-safe! source source-start target target-start len)
  (with-resizing-bv target (lambda (target) (bytevector-copy! source source-start target target-start len))))

(define* (bytevector-uint-set-safe! bv index value size
                                    optional: (e (endianness big)))
  (with-resizing-bv bv (lambda (bv) (bytevector-uint-set! bv index value e size))))

(define ((u8! item) bv ptr)
  (values #f
          (bytevector-u8-set-safe! bv ptr item)
          (+ ptr 1)))

(define* ((u16! item optional: (e (endianness big))) bv ptr)
  (values #f
          (bytevector-u16-set-safe! bv ptr item e)
          (+ ptr 2)))

(define* ((u32! item optional: (e (endianness big))) bv ptr)
  (values #f
          (bytevector-u32-set-safe! bv ptr item e)
          (+ ptr 4)))

(define* ((uint-set! value size optional: (e (endianness big))) bv ptr)
  (values #f
          (bytevector-uint-set-safe! bv ptr value size e)
          (+ ptr size)))

(define* ((bv-copy! source
                    optional:
                    (start 0)
                    (length (- (bytevector-length source) start)))
          bv ptr)
  (values #f
          (bytevector-copy-safe! source start bv ptr length)
          (+ ptr length)))
