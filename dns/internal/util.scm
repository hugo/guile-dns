(define-module (dns internal util)
  :export (-> ->> repeat flip))

(define-syntax ->
  (syntax-rules ()
    [(-> obj) obj]
    [(-> obj (func args ...) rest ...)
     (-> (func obj args ...) rest ...)]
    [(-> obj func rest ...)
     (-> (func obj) rest ...)]))

(define-syntax ->>
  (syntax-rules ()
    ((->> obj)
     obj)
    ((->> obj (func args ...) rest ...)
     (->> (func args ... obj) rest ...))
    ((->> obj func rest ...)
     (->> (func obj) rest ...))))



(define (repeat times proc)
  (map (lambda _ proc)
       (iota times)))

(define (flip f)
  (lambda args
    (apply f (reverse args))))
