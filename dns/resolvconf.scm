(define-module (dns resolvconf)
  :use-module (ice-9 regex)
  :use-module (ice-9 rdelim)
  :export (resolvconf))


(define (regexp-exec/all rx str)
  (let loop ((start 0))
    (cond ((regexp-exec rx str start)
           => (lambda (m) (cons m (loop (match:end m)))))
          (else '()))))


(define rx (make-regexp "^nameserver\\s+(\\S+)\\s*$" regexp/newline))
(define* (resolvconf #:optional (fname "/etc/resolv.conf"))
  (map (lambda (m) (match:substring m 1))
       (regexp-exec/all rx (call-with-input-file fname read-string))))

