(define-module (dns types rr)
  :use-module (srfi srfi-88)
  :use-module (dns internal util)
  :use-module (dns internal object)
  :use-module (dns internal state-monad)
  :use-module (dns internal bv)
  :use-module (dns util)
  :use-module (dns label)
  :use-module (dns enum)
  :use-module (dns types)
  :use-module (dns types rr-data)
  :use-module ((rnrs bytevectors) :select (bytevector? bytevector-length))
  :re-export (name type class ttl rdata)
  :export (make-dns-rr-data
           dns-rr-data?
           bytes->rr-data
           rr-data->bytes))

(define-record-type dns-rr-data
  (fields (name type: string?)
          (type type: (or u16? (assq (@ (dns enum) rr-types))))
          (class
            default: 'IN
            type: (or u16? (assq (@ (dns enum) class-types))))
          (ttl type: (uint? 32))
          ;; Rdata's type is dependant on the type field, and is therefore left blank.
          rdata))


(define (bytes->rr-data bv)
  (do
    name     <- (<$> domain-join (labels->string-list bv))
    type     <- (<$> int->rr (u16 bv))
    class    <- (<$> int->class (u16 bv))
    ttl      <- (u32 bv)
    rdlength <- (u16 bv)
    rdata    <- ((get-deserializer type) bv rdlength)
    (return (make-dns-rr-data name: name type: type class: class
                              ttl: ttl rdata: rdata))))


(define (rr-data->bytes rr)
  (do
      (bv-copy! (string-list->labels (string-split (name rr) #\.))) ; name
      (u16! (rr->int (type rr)))        ; type
    (u16! (class->int (class rr)))      ; class
    (u32! (ttl rr))                     ; ttl
    ;; rdlength and rdata
    (if (bytevector? (rdata rr))
        (do (u16! (bytevector-length (rdata rr)))
            (bv-copy! (rdata rr)))
        ((get-serializer (type rr)) (rdata rr)))))
