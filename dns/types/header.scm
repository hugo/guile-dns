(define-module (dns types header)
  :use-module (srfi srfi-88)
  :use-module (rnrs bytevectors)
  :use-module (dns internal util)
  :use-module (dns internal state-monad)
  :use-module (dns internal bv)
  :use-module (dns internal object)
  :use-module (dns enum)
  :use-module (dns types)
  :use-module (dns util)
  :re-export (id qr opcode aa tc rd ra z rcode qdcount ancount nscount arcount)
  :export (make-dns-header
           dns-header?

           dns-header->bytes
           bytes->dns-header

           encode-dns-header-flags
           decode-dns-header-flags
           ))


(define-record-type dns-header
  (fields (id default: 0 type: u16?)
          ;; flags
          (qr type: boolean?)
          (opcode default: 'QUERY
                  type: (or (uint? 4) (assq (@ (dns enum) opcode-types))))
          (aa type: boolean?) (tc type: boolean?) (rd type: boolean?) (ra type: boolean?)
          (z default: 0)
          (rcode default: 'NOERROR type: (or (uint? 4) (assq (@ (dns enum) rcode-types))))
          ;; lengths
          (qdcount default: 0 type: u16?) (ancount default: 0 type: u16?)
          (nscount default: 0 type: u16?) (arcount default: 0 type: u16?)))


(define (encode-dns-header-flags msg)
  (logior
    (if (qr msg) (ash 1 15) 0)
    (cond ((opcode msg) => (lambda (op) (ash (opcode->int op) 11))) (else 0))
    (if (aa msg) (ash 1 10) 0)
    (if (tc msg) (ash 1 9) 0)
    (if (rd msg) (ash 1 8) 0)
    (if (ra msg) (ash 1 7) 0)
    (cond ((z msg) => (lambda (z) (ash z 4))) (else 0))
    (cond ((rcode msg) => rcode->int) (else 0))))

(define (decode-dns-header-flags u16)
  (define (byte x)
    (not (zero? (logand u16 (ash 1 x)))))
  (list
    (byte 15)                           ; qr
    (logand #xF (ash u16 -11))          ; opcode
    (byte 10)                           ; aa
    (byte 9)                            ; tc
    (byte 8)                            ; rd
    (byte 7)                            ; ra
    (logand 7 (ash u16 -4))             ; z
    (logand #xF u16)))                  ; rcode

(define* (dns-header->bytes msg)
  (do
      (u16! (or (id msg) 0))
      (u16! (encode-dns-header-flags msg))
    (u16! (or (qdcount msg) 0))
    (u16! (or (ancount msg) 0))
    (u16! (or (nscount msg) 0))
    (u16! (or (arcount msg) 0))))

(define (bytes->dns-header bv)
  (do
    id <- (u16 bv)
    flags <- (<$> decode-dns-header-flags (u16 bv))
    qd <- (u16 bv)
    an <- (u16 bv)
    ns <- (u16 bv)
    ar <- (u16 bv)
    (return (make-dns-header
             id: id
             qr:     (list-ref flags 0)
             opcode: (int->opcode (list-ref flags 1))
             aa:     (list-ref flags 2)
             tc:     (list-ref flags 3)
             rd:     (list-ref flags 4)
             ra:     (list-ref flags 5)
             z:      (list-ref flags 6)
             rcode:  (int->rcode (list-ref flags 7))
             qdcount: qd
             ancount: an
             nscount: ns
             arcount: ar))))
