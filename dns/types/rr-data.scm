(define-module (dns types rr-data)
  :use-module (dns internal state-monad)
  :use-module (dns internal bv)
  :use-module (dns label)
  :use-module (dns util)

  :use-module (rnrs bytevectors)

  :export (register-rr-handler!
           get-serializer
           get-deserializer
           not-implemented))

(define-once handlers (make-hash-table))

(define (augument-with-length serializer)
  "Returns a new serializer which runs the given serializer, but prepends a u16
of the length of the serialized data."
  (lambda (rr)
   (do (bv* ptr*) <- (get)
       (put bv* (+ ptr* 2))

       (serializer rr)

       (_ new) <- (get)
       len = (- new ptr* 2)
       (bv _) <- (get)

       ;; technically we have to save the updated bv which u16! returns.
       ;; However, since we write to a earlier point we KNOW that it wont
       ;; be resized, meaning that our bytevector will be unchanged
       (with-temp-state (list bv ptr*) (u16! len)))))


(define (register-rr-handler! symbol serializer deserializer)
  "Adds a serializer and deserializer associated with symbol
The serializers and deserializers must have the types

serializer   :: rr → bv x ptr → _ x bytevector x ptr
serializer   :: rr → State (bv, ptr) _

deserializer :: bv → ptr → <value> x ptr
deserializer :: bv → State ptr <value>
"
  (if (list? symbol)
      (for-each (lambda (symb) (register-rr-handler! symb serializer deserializer))
                symbol)
      (hashq-set! handlers symbol
                  (cons (augument-with-length serializer)
                        deserializer))))

(define (get-serializer symbol)
  (cond ((hashq-ref handlers symbol) => car)
        (else (scm-error 'misc-error "get-serializer"
                         "No serializer for type ~a"
                         (list symbol) #f))))

(define (get-deserializer symbol)
  (cond ((hashq-ref handlers symbol) => cdr)
        ;; If no deserializer is known, just copy the bytes
        ;; TODO write a test for this
        (else bv-get)))

(define (not-implemented str)
  (lambda _ (return (scm-error 'not-implemented #f str #f #f))))



(register-rr-handler!
 'A
 (lambda (data) (u32! (inet-pton AF_INET data)))
 (lambda (bv len) (<$> (lambda (n) (inet-ntop AF_INET n)) (u32 bv))))

(register-rr-handler!
 'AAAA
 (lambda (data) (uint-set! (inet-pton AF_INET6 data) (/ 128 8)))
 (lambda (bv len) (<$> (lambda (n) (inet-ntop AF_INET6 n))
              (uint-get bv (/ 128 8)))))

(register-rr-handler!
 '(NS CNAME PTR)
 (lambda (data) (bv-copy! (string-list->labels (string-split data #\.))))
 (lambda (bv len)
   (do labels <- (labels->string-list bv)
       (return (string-join labels ".")))))

(register-rr-handler!
 'TXT
 (lambda (data) (bv-copy! (string-list->labels (list data))))
 (lambda (bv len)
   (do labels <- (labels->string-list bv)
       (return (string-join labels ".")))))

(register-rr-handler!
 'SOA
 (lambda (data)
   (call-with-values (lambda () (apply values data))
     (lambda (mname rname serial refresh retry expire nttl)
       (do
           (bv-copy! (string-list->labels (string-split mname #\.)))
           (bv-copy! (string-list->labels (string-split rname #\.)))
         (u32! serial)
         (u32! refresh)
         (u32! retry)
         (u32! expire)
         (u32! nttl)))))
 (lambda (bv len)
  (do
      mname   <- (<$> domain-join (labels->string-list bv))
      rname   <- (<$> domain-join (labels->string-list bv))
      serial  <- (u32 bv)
      refresh <- (u32 bv)
      retry   <- (u32 bv)
      expire  <- (u32 bv)
      nttl    <- (u32 bv)
      (return (list mname rname serial refresh retry expire nttl)))))

(register-rr-handler!
 'HINFO
 (lambda (data)
   (do (bv-copy! (string-list->labels (string-split (list-ref data 0) #\.)))
       (bv-copy! (string-list->labels (string-split (list-ref data 1) #\.)))))
 (lambda (bv len)
  (lift list
        (<$> domain-join (labels->string-list bv))
        (<$> domain-join (labels->string-list bv)))))

(register-rr-handler!
 'MX
 (lambda (data)
   (do (u16! (list-ref data 0))
       (bv-copy! (string-list->labels (string-split (list-ref data 1) #\.)))))
 (lambda (bv len) (lift list (u16 bv) (<$> domain-join (labels->string-list bv)))))

;; priority weight port target
(register-rr-handler!
 'SRV
 (not-implemented "SRV serializer")
 (lambda (bv len)
   (do priority <- (u16 bv)
       weight   <- (u16 bv)
       port     <- (u16 bv)
       target   <- (<$> domain-join (labels->string-list bv))
       (return (list priority weight port target)))))
