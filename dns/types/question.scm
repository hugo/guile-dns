(define-module (dns types question)
  :use-module (srfi srfi-88)
  :use-module (rnrs bytevectors)
  :use-module (dns internal util)
  :use-module (dns internal state-monad)
  :use-module (dns internal bv)
  :use-module (dns internal object)
  :use-module (dns util)
  :use-module (dns label)
  :use-module (dns enum)
  :use-module (dns types)
  :use-module (dns types rr)
  :re-export (name type class)
  :export (make-dns-question
           dns-question?

           dns-question->bytes
           bytes->dns-question
           ))


(define-record-type dns-question
  (fields (name type: string?)
          (type type: (or u16? (assq (@ (dns enum) rr-types))))
          (class default: 'IN type: (or u16? (assq (@ (dns enum) class-types))))))

(define (dns-question->bytes q)
  (do (bv-copy! (-> q name (string-split #\.) string-list->labels))
      (u16! (rr->int (type q)))
    (u16! (class->int (class q)))))

(define (bytes->dns-question bv)
  (do
    name <- (labels->string-list bv)
    type <- (<$> int->rr (u16 bv))
    class <- (<$> int->class (u16 bv))
    (return (make-dns-question name: (string-join name ".") type: type class: class))))
