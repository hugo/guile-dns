(define-module (dns types message)
  :use-module (srfi srfi-88)
  :use-module ((srfi srfi-1) :select (every))
  :use-module (rnrs lists)
  :use-module (rnrs bytevectors)
  :use-module (dns internal util)
  :use-module (dns internal state-monad)
  :use-module (dns internal bv)
  :use-module (dns internal object)

  :use-module (dns types)
  :use-module (dns types header)
  :use-module (dns types question)
  :use-module (dns types rr)

  :re-export (header questions answers authorities additionals)
  :export (make-dns-message
           dns-message?
           bytes->dns-message dns-message->bytes
           ))

(define (rr-list? x)
  (and list? (every dns-rr-data? x)))

(define-record-type dns-message
  (fields (header type: dns-header?)
          (questions   default: '()
                       type: (and list? ((flip every) dns-question?)))
          (answers     default: '() type: rr-list?)
          (authorities default: '() type: rr-list?)
          (additionals default: '() type: rr-list?)))


(define (dns-message->bytes msg)
  (do (dns-header->bytes (header msg))
      (sequence (map dns-question->bytes (questions msg)))
    (sequence (map rr-data->bytes (answers msg)))
    (sequence (map rr-data->bytes (authorities msg)))
    (sequence (map rr-data->bytes (additionals msg)))))

(define (bytes->dns-message bv)
  (do
    header <- (bytes->dns-header bv)
    qs <- (sequence
            (repeat (qdcount header)
                    (bytes->dns-question bv)))
    an <- (sequence
            (repeat (ancount header)
                    (bytes->rr-data bv)))
    ns <- (sequence
            (repeat (nscount header)
                    (bytes->rr-data bv)))
    ar <- (sequence
            (repeat (arcount header)
                    (bytes->rr-data bv)))
    (return (make-dns-message header: header questions: qs answers: an
                              authorities: ns additionals: ar))))
