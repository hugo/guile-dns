(define-module (dns label)
  :use-module (rnrs io ports)
  :use-module (dns internal util)
  :use-module (dns internal state-monad)
  :use-module (dns internal bv)
  :use-module (rnrs bytevectors)
  :export (string-list->labels labels->string-list
           ))

;; Encodes the scheme list of strings into a bytevector on the label format
;; expected by the DNS wire protocol.
;; TODO shouldn't this be in the state monad?
(define (string-list->labels lst*)
  (define trans (native-transcoder))
  (define lst
    (let ((last (last-pair lst*)))
      (cond ((null? last) '(""))
            ((string-null? (car last)) lst*)
            (else (append lst* '(""))))))
  (call-with-values (lambda () (open-bytevector-output-port))
                    (lambda (port get-bytevector)
                      (for-each (lambda (s)
                                  (define bv (string->bytevector s trans))
                                  (put-u8 port (bytevector-length bv))
                                  (put-bytevector port bv))
                                lst)
                      (get-bytevector))))

;; Is the byte starting this label a pointer somewhere else?
(define (label-pointer? byte)
  (= 3 (ash byte -6)))

(define (labels->string-list bv)
  (do len <- (u8 bv)
    (cond ((label-pointer? len)
           (do
             (mod 1-) ; unreads first char
             ptr* <- (u16 bv)
             (with-temp-state (list (logand ptr* #x1FFF))
                              (labels->string-list bv))))
          ((zero? len) (return '()))
          (else
            (do
              this <- (bv-get bv len)
              rest <- (labels->string-list bv)
              (return
                (cons (bytevector->string this (native-transcoder))
                      rest)))))))

