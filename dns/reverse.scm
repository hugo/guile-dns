(define-module (dns reverse)
  :use-module ((srfi srfi-1) :select (iota))
  :use-module (srfi srfi-71)
  :use-module (srfi srfi-88)
  :use-module ((dns util) :select (domain-join))
  :export (ipv4-address?
           ipv6-address?
           guess-address-type
           reverse-address))



;; How many bits wide is the address
(define (addr-width fam)
  (cond ((= AF_INET fam) 32)
        ((= AF_INET6 fam) 128)))

;; How many bits is the smallest unit of the address
(define (group-width fam)
  (cond ((= AF_INET fam) 8)
        ((= AF_INET6 fam) 4)))

;; How many groups of the smalest possible size is in the address
(define (group-count fam)
  (/ (addr-width fam)
     (group-width fam)))

;; Which (numerical) base is the address written in
(define (addr-base fam)
  (cond ((= AF_INET fam) 10)
        ((= AF_INET6 fam) 16)))

;; What suffix is used for a reverse address of the family.
(define (reverse-suffix fam)
  (cond ((= AF_INET fam) "in-addr")
        ((= AF_INET6 fam) "ip6")))

(define (family-name fam)
  (cond ((= AF_INET fam) "IPv4")
        ((= AF_INET6 fam) "IPv6")
        (else "UNKNOWN")))



(define (ipv4-address? string)
  (catch 'misc-error (lambda () (inet-pton AF_INET string))
    (const #f)))

(define (ipv6-address? string)
  (catch 'misc-error (lambda () (inet-pton AF_INET6 string))
    (const #f)))

(define (guess-address-type string)
  (cond ((ipv4-address? string) => (lambda (n) (values AF_INET n)))
        ((ipv6-address? string) => (lambda (n) (values AF_INET6 n)))
        (else #f)))


(define* (reverse-address address optional: address-family)
  (let ((address-family
         numeric-address
         (if address-family
             (values address-family
                     (catch 'misc-error
                       (lambda () (inet-pton address-family address))
                       (lambda _
                         (scm-error 'wrong-type-arg "reverse-address"
                                    "Value can't be parsed as an ~a address: ~s"
                                    (list (family-name address-family) address)
                                    #f))))
             (call-with-values (lambda () (guess-address-type address))
               (case-lambda
                 ((family address) (values family address))
                 ((_) (scm-error 'wrong-type-arg "reverse-address"
                                 "Value can't be parsed as an address: ~s"
                                 (list address) #f)))))))

    (domain-join
     (append
      (map (lambda (x)
             (number->string
              (logand (1- (expt 2 (group-width address-family)))
                      (ash numeric-address x))
              (addr-base address-family)))
           (iota (group-count address-family)
                 0
                 (- (group-width address-family))))
      (list (reverse-suffix address-family) "arpa")))))

