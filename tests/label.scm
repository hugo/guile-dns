(use-modules (srfi srfi-64)
             (rnrs io ports)
             (dns label))

(define codec (make-transcoder (latin-1-codec)))

(test-begin "DNS labels")

(test-equal "Empty list to null label"
  #vu8(0)
  (string-list->labels '()))

(test-equal "Empty label set gives empty list"
  '() ((labels->string-list #vu8(0)) 0))

(let* ((bv (string->bytevector "\x05Hello\0" codec)))
  (test-equal "Single string to label"
    bv (string-list->labels (list "Hello")))
  (test-equal "Single label to strirng"
    '("Hello")
    ((labels->string-list bv) 0)))

(let* ((bv (string->bytevector "\x05Hello\x04Test\0" codec)))
  (test-equal "Actual list to labels"
    bv (string-list->labels (list "Hello" "Test")))
  (test-equal "Multiple labels to list"
    '("Hello" "Test")
    ((labels->string-list bv) 0)))

(let* ((bv (string->bytevector "\x05Hello\x05Hello\x05Hello\0" codec)))
  (test-equal "Test that pointers are still not implented"
    bv (string-list->labels (list "Hello" "Hello" "Hello"))))

(let* ((bv (string->bytevector "\xC0\x02\x05Hello\0" codec)))
  (test-equal "Test simple label pointer"
    '("Hello")
    ((labels->string-list bv) 0)))

(let* ((bv (string->bytevector "\x05Hello\0\xC0\x00" codec)))
  (test-equal "Test pointer to label before start."
      '("Hello")
      ((labels->string-list bv)
       7 ; Index of \xC0
       )))

(test-end)
