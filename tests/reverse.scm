(use-modules (srfi srfi-64)
             (dns reverse))

(test-begin "Reverse addresses")

;; Only some simple tests of guess-address-type, since all intriciacies are
;; handled by inet_pton(3). Techinically `ipv{4,6}-address?' should be tested
;; separately, but that's skipped since guess-address-type simply calls them.

(call-with-values (lambda () (guess-address-type "10.20.30.40"))
  (case-lambda
    ((type value)
     (test-equal "Guessed AF_INET" AF_INET type)
     (test-equal "Also parsed IPv4 address" 169090600 value))
    ((_) (test-assert "Parse IPv4 failed" #f))))

(call-with-values (lambda () (guess-address-type "::"))
  (case-lambda
    ((type value)
     (test-equal "Guessed AF_INET6" AF_INET6 type)
     (test-equal "Also parsed IPv6 address" 0 value))
    ((_) (test-assert "Parse IPv6 failed" #f))))

(call-with-values (lambda () (guess-address-type "::FFFF:16.32.64.128"))
  (case-lambda
    ((type value)
     (test-equal "Guessed AF_INET6 for embedded IPv4" AF_INET6 type)
     (test-equal "Parsed embedded IPv4 correctly" #xFFFF10204080 value))))


(test-assert "Guess-address-type should fail for something not a valid IP address"
  (not (guess-address-type "10")))



(test-equal "reverse-address with automatically detected IPv4"
  "40.30.20.10.in-addr.arpa"
  (reverse-address "10.20.30.40"))


(test-equal "reverse-address for explicitly mentioned AF_INET"
  "40.30.20.10.in-addr.arpa"
  (reverse-address "10.20.30.40" AF_INET))

(test-equal "reverse-address with automatically detected IPv6"
  "0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa"
  (reverse-address "::"))

(test-equal "reverse-address for explicitly mentioned AF_INET6"
  "0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa"
  (reverse-address "::" AF_INET6))

(catch 'wrong-type-arg
  (lambda ()
    (reverse-address "::" AF_INET)
    (test-assert "reverse-address didn't fail for an invalid family" #f))
  (lambda _
    (test-assert "reverse-address fails for an invalid family" #t)))

(catch 'wrong-type-arg
  (lambda ()
    (reverse-address "Invalid")
    (test-assert "reverse-address didn't fail for an undeterminable address" #f))
  (lambda _
    (test-assert "reverse-address fails when family can't be determined" #t)))

(test-end)
