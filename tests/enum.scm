(use-modules (srfi srfi-64)
             (dns enum))

;;; These tests claim to be testing rr->int, but is really testing make-mappings.
;;; Explicit data could be checked, but a test for that would just be duplicating the data

(test-begin "Enumerations")

(test-assert "rr->int returns a number for a valid symbol" (integer? (rr->int 'A)))

(let ((r (rr->int 10)))
  (test-assert "rr->int returns an integer given an integer" (integer? r))
  (test-assert "rr->int returns an exact integer given an exact integer" (exact? r)))

(let ((r (rr->int 10.0)))
  (test-assert "rr->int returns an integer given an inexact integer" (integer? r))
  (test-assert "rr->int returns an exact integer an inexact integer" (exact? r)))

(catch 'wrong-type-arg
  (lambda ()
    (rr->int 'INVALID-SYMBOL)
    (test-assert "rr->int didn't fail on invalid symbol" #f))
  (lambda _ (test-assert "rr->int fails on an unknown symbol" #t)))


(test-equal "Int->rr returns a symbol for a known value" 'A (int->rr 1))
(define first-private 65280)
(test-equal "Int->rr is effectivly `identity' for unknown integers"
  first-private (int->rr first-private))
(test-equal "Int->rr is effectivly `inexact->exact' for unknown inexact integers" 100 (int->rr 100.0))
(catch 'wrong-type-arg
  (lambda ()
    (int->rr 27.5)
    (test-assert "Int->rr didn't fail on invalid data" #f))
  (lambda (err proc fmt args data)
    (test-assert "Int->rr failed on invalid data" #t)
    (test-assert "Int->rr returned a list of valid symbols as aux data upon failure"
      (every symbol? (car data)))))

(test-end)
