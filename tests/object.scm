(use-modules (srfi srfi-64)
             (srfi srfi-88)
             (dns internal object))

(test-begin "Object system")

(define-record-type r
  (fields y))

(test-assert "Constructor created for custom type" (procedure? make-r))
(test-assert "Predicate created for custom type" (procedure? r?))
(test-assert "Acessor created for custom type" (procedure? y))
(test-equal "Custom type make/get round-trip" 10 (y (make-r y: 10)))


(define-record-type r2
  (fields (x default: 10)))

(test-equal "Custom type constructor initial value" 10 (x (make-r2)))

(define-record-type r3
  (fields (x type: integer? default: 20)))

(test-equal "Custom type constructor initial value when not first argument"
  20 (x (make-r3)))

(define-record-type r4
  (fields (x type: integer?)))

;; It's undecided if the default value for a field without an explicit default
;; value is undefined or false, but this at least tests that it doesn't suddenly
;; change.
(test-equal "Undefined non-defaulted fields default to #f"
  #f (y (make-r)))

;; This is however explicitly specified
(test-equal "Test default value for non-defaulted file with other kv-data"
  #f (x (make-r4)))




(catch 'wrong-type-arg
  (lambda () (make-r4 x: "Hello")
     (test-assert "Type test didn't work" #f))
  (lambda _ (test-assert "Type test failed correctly" #t)))

(test-end)
