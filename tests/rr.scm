(use-modules (srfi srfi-64)
             (rnrs bytevectors)
             (rnrs io ports)
             (dns types rr-data))

(test-begin "DNS Resource Records")

(define (bytevector-subvector bv from len)
  (let ((bv* (make-bytevector len)))
    (bytevector-copy! bv from
                      bv* 0
                      len)
    bv*))

(define (test-serialize type src expected)
  (call-with-values
      (lambda ()
        (((get-serializer type) src)
         (make-bytevector 100)
         0))
    (lambda (_ bv ptr)
      ;; (test-equal "Ptr incremented correctly" 6 ptr)
      (test-equal "Data written correctly"
        expected
        (bytevector-subvector bv 0 ptr)))))

(define (test-deserialize type src expected)
 (call-with-values
     (lambda ()
       (((get-deserializer type) src (bytevector-length src))
        0))
   (lambda (str ptr)
     (test-equal "Data parsed correctly"
       expected str))))


(test-serialize   'A "10.20.30.40" #vu8(0 4 10 20 30 40))
(test-deserialize 'A #vu8(10 20 30 40) "10.20.30.40")

(test-serialize   'AAAA "::1" #vu8(0 16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1))
(test-deserialize 'AAAA #vu8(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1) "::1")

(define codec (make-transcoder (latin-1-codec)))
(define (s->bv s) (string->bytevector s codec))

(test-serialize 'TXT "Hello, World!" (s->bv "\x00\x0F\x0DHello, World!\x00"))
(test-deserialize 'TXT (s->bv "\x05Hello\0") "Hello")
(test-deserialize 'TXT (s->bv "\x05Hello\x05World\0") "Hello.World")

(let ((soa-bytes
       #vu8(                    ;;
            3 110 115 49        ; ns1
            6 97 100 114 105 102 116    ; adrift
            5 115 112 97 99 101         ; space
            0
            4 104 117 103 111                ; hugo
            9 104 111 114 110 113 117 105 115 116 ; hornquist
            2 115 101                             ; se
            0
            120 134 209 52              ; serial
            0 18 117 0                  ; refresh
            0 0 28 32                   ; retry
            0 54 238 128                ; expire
            0 0 1 44                    ; nttl
            ))

      (soa-record (list "ns1.adrift.space"
                        "hugo.hornquist.se"
                        2022101300
                        1209600
                        7200
                        3600000
                        300)))

  (test-serialize 'SOA soa-record
                  (let* ((len (bytevector-length soa-bytes))
                         (bv (make-bytevector (+ 2 len))))
                    (bytevector-copy! soa-bytes 0 bv 2 len)
                    (bytevector-u16-set! bv 0 len (endianness big))
                    bv))
  (test-deserialize 'SOA soa-bytes soa-record))


(test-end)
