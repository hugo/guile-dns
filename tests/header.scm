(use-modules (srfi srfi-64)
             (srfi srfi-88)
             (dns types header))

(test-begin "DNS message header")

(test-equal 256 (encode-dns-header-flags (make-dns-header rd: #t)))

(test-equal '(#f 0 #f #f #t #f 0 0)
  (decode-dns-header-flags 256))

;; TODO more tests here
;;
;; TODO also test all the something->bytevector and back procedures


(test-end)
