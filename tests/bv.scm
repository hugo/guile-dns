(use-modules (srfi srfi-64)
             (srfi srfi-1)
             (rnrs bytevectors)
             (rnrs io ports)
             (dns internal bv)
             (dns internal state-monad))

(test-begin "Bytevectors in state monad")


(define bv (string->bytevector "Hello, World"
                               (make-transcoder (utf-8-codec))))


;; Returns in `do' allow arbitrary code to be run for side effects
;; order is guaranteed to be maintained, since each procedure in a
;; state monad depends on the procedures before it.
((do c1 <- (u8 bv)
     (return (test-equal "Get first u8 from bv" (char->integer #\H) c1))
     c2 <- (u8 bv)
     (return (test-equal "Get second u8 from bv" (char->integer #\e) c2))
     i <- (u16 bv)
     (return (test-equal "Get u16 (endianess unspecified)"
               (let ((l (char->integer #\l)))
                 (logior (ash l 8) l))
               i))
     j <- (u32 bv)
     (return (test-equal "Get u32, and check that endianess is big"
               (apply logior
                      (map ash (map char->integer (string->list "o, Wo"))
                           (reverse (iota 4 0 8))))
               j))
     (return #f))
 0)

(test-equal "Bv-get"
  #vu8(2 3)
  ((bv-get #vu8(1 2 3 4) 2) 1))


(test-equal "Uint-get"
  #x010203 ((uint-get #vu8(1 2 3 4) 3) 0))
(test-equal "Uint-get, with an offset"
  #x020304 ((uint-get #vu8(1 2 3 4) 3) 1))


(test-end)
