(use-modules (srfi srfi-64)
             (srfi srfi-71)
             (srfi srfi-88)
             (rnrs bytevectors)
             (dns types header)
             (dns types message)
             (dns types question)
             (dns types rr))

(test-begin "Encode-decode round trip for all types")

(define header (make-dns-header qdcount: 1
                                ancount: 1))
(define question (make-dns-question name: "www.lysator.liu.se"
                                    type: #xABCD))
(define rr (make-dns-rr-data name: "www.lysator.liu.se"
                             type: 'A
                             ttl: 300
                             rdata: "10.20.30.40"))
(define message (make-dns-message header: header
                                  questions: (list question)
                                  answers: (list rr)))


(let ((_ bv ptr ((dns-header->bytes header)
                 (make-bytevector 0)
                 0)))
  (let ((header* _ ((bytes->dns-header bv) 0)))
    (test-equal "Header surrive round-trip" header header*)))


(let ((_ bv ptr ((dns-question->bytes question)
                 (make-bytevector 0) 0)))
  (let ((question* _ ((bytes->dns-question bv) 0)))
    (test-equal "Question surrive round-trip" question question*)))

(let ((_ bv ptr ((rr-data->bytes rr)
                 (make-bytevector 0) 0)))
  (let ((rr* _ ((bytes->rr-data bv) 0)))
    (test-equal "RR surrive round-trip" rr rr*)))


(let ((_ bv ptr ((dns-message->bytes message)
                 (make-bytevector 0) 0)))
  (let ((message* _ ((bytes->dns-message bv) 0)))
    (test-equal "DNS message surrive round-trip" message message*)))


(test-end)
