Guile DNS
=========

A library for DNS interactions in pure Guile Scheme.

Documentation is available in `doc/guile-dns.texi`, and a sample client
is available in `sample-client.scm`.

Contributions are either welcome at https://git.lysator.liu.se/hugo/guile-dns or
through email to hugo@lysator.liu.se.

The software is licensed under GPLv3 or (at your option) any later version.
